# README #

Firmware for a PIC32 based SLCAN adapter.

### What is this repository for? ###

* Quick summary
 Firmware for a Microchip PIC32 SLCAN adapter.
 Provides USB ACM serial interface to CAN bus via SLCAN command set.
 
* Version 1.0
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###
Required tools:
* MPLABX 1.4 or higher
* MPLAB Harmony 2.x
* XC32 compiler or other suitable tool chain.


* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

vadim at interface-devices.com
