/* 
 * File:   can232.h
 * Author: vadim
 *
 * Created on 16 March 2016, 12:16
 */

#ifndef SLCAN_H
#define	SlCAN_H

#include <stdint.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdlib.h>

#ifdef	__cplusplus
extern "C" {
#endif

//#define SLCAN_ID CAN_ID_1
//#define SLCAN_CH_TX CAN_CHANNEL2
//#define SLCAN_CH_RX CAN_CHANNEL3
    
    
#define SLCAN_VERSION_STR     "V1013"
#define SLCAN_SERIAL_NUM      "NA123"

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//   Commands not supported/not implemented:
//     s, W, M, m, U
//      
//   Commands modified:
//     S - supports not declared 83.3 rate straight away (S9) fork with 83.3 support, easy to add.
//     F - returns error flags
//     Z - extra Z2 option enables 4 byte timestamp vs standard 2 byte (60000ms max)
//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                   
//                          CODE   SUPPORTED   SYNTAX               DESCRIPTION     
//
#define SLCAN_CMD_SETUP     'S' //   YES+      Sn[CR]               Setup with standard CAN bit-rates where n is 0-8.
                                //                                  S0 10Kbit          S4 125Kbit         S8 1Mbit
                                //                                  S1 20Kbit          S5 250Kbit         S9 83.3Kbit
                                //                                  S2 50Kbit          S6 500Kbit
                                //                                  S3 100Kbit         S7 800Kbit
#define SLCAN_CMD_SETUP_BTR 's' //    -        sxxyy[CR]            Setup with BTR0/BTR1 CAN bit-rates where xx and yy is a hex value.
#define SLCAN_CMD_OPEN      'O' //   YES       O[CR]                Open the CAN channel in normal mode (sending & receiving).
#define SLCAN_CMD_LISTEN    'L' //   YES       L[CR]                Open the CAN channel in listen only mode (receiving).
#define SLCAN_CMD_CLOSE     'C' //   YES       C[CR]                Close the CAN channel.
#define SLCAN_CMD_TX11      't' //   YES       tiiildd...[CR]       Transmit a standard (11bit) CAN frame.
#define SLCAN_CMD_TX29      'T' //   YES       Tiiiiiiiildd...[CR]  Transmit an extended (29bit) CAN frame
#define SLCAN_CMD_RTR11     'r' //   YES       riiil[CR]            Transmit an standard RTR (11bit) CAN frame.
#define SLCAN_CMD_RTR29     'R' //   YES       Riiiiiiiil[CR]       Transmit an extended RTR (29bit) CAN frame.
#define SLCAN_CMD_POLL_ONE  'P' //   YES       P[CR]                Poll incomming FIFO for CAN frames (single poll)
#define SLCAN_CMD_POLL_MANY 'A' //   YES       A[CR]                Polls incomming FIFO for CAN frames (all pending frames)
#define SLCAN_CMD_FLAGS     'F' //   YES+      F[CR]                Read Status Flags.
#define SLCAN_CMD_AUTOPOLL  'X' //   YES       Xn[CR]               Sets Auto Poll/Send ON/OFF for received frames.
#define SLCAN_CMD_FILTER    'W' //    -        Wn[CR]               Filter mode setting. By default CAN232 works in dual filter mode (0) and is backwards compatible with previous CAN232 versions.
#define SLCAN_CMD_ACC_CODE  'M' //    -        Mxxxxxxxx[CR]        Sets Acceptance Code Register (ACn Register of SJA1000). // we use MCP2515, not supported
#define SLCAN_CMD_ACC_MASK  'm' //    -        mxxxxxxxx[CR]        Sets Acceptance Mask Register (AMn Register of SJA1000). // we use MCP2515, not supported
#define SLCAN_CMD_UART      'U' //   YES       Un[CR]               Setup UART with a new baud rate where n is 0-6.
#define SLCAN_CMD_VERSION1  'V' //   YES       v[CR]                Get Version number of both CAN232 hardware and software
#define SLCAN_CMD_VERSION2  'v' //   YES       V[CR]                Get Version number of both CAN232 hardware and software
#define SLCAN_CMD_SERIAL    'N' //   YES       N[CR]                Get Serial number of the CAN232.
#define SLCAN_CMD_TIMESTAMP 'Z' //   YES+      Zn[CR]               Sets Time Stamp ON/OFF for received frames only. EXTENSION to LAWICEL: Z2 - millis() timestamp w/o standard 60000ms cycle
#define SLCAN_CMD_AUTOSTART 'Q' //   YES  todo     Qn[CR]               Auto Startup feature (from power on). 

#define LOW_BYTE(x)     ((unsigned char)((x)&0xFF))
#define HIGH_BYTE(x)    ((unsigned char)(((x)>>8)&0xFF))
#define LOW_WORD(x)     ((unsigned short)((x)&0xFFFF))
#define HIGH_WORD(x)    ((unsigned short)(((x)>>16)&0xFFFF))

#ifndef INT32U
#define INT32U uint32_t
#endif

#ifndef INT16U
#define INT16U uint16_t
#endif

#ifndef INT8U
#define INT8U uint8_t
#endif

#define SLCAN_OK                      0x00
#define SLCAN_OK_SMALL                0x01
#define SLCAN_OK_BIG                  0x02
#define SLCAN_ERR                     0x03
#define SLCAN_ERR_NOT_IMPLEMENTED     0x04
#define SLCAN_ERR_UNKNOWN_CMD         0x05

#define SLCAN_FILTER_SKIP			  0x00
#define SLCAN_FILTER_PROCESS	      0x01

//#define SLCAN_IS_OK(x) ((x)==SLCAN_OK ||(x)==SLCAN_OK_NEW ? TRUE : FALSE)

#define SLCAN_CR    '\r'
#define SLCAN_ALL   'A'
#define SLCAN_FLAG  'F'
#define SLCAN_TR11  't'
#define SLCAN_TR29  'T'

#define SLCAN_RET_ASCII_OK             0x0D
#define SLCAN_RET_ASCII_ERROR          0x07
#define SLCAN_RET_ASCII_OK_SMALL       'z'
#define SLCAN_RET_ASCII_OK_BIG         'Z'

#define SLCAN_STATUS_CAN_CLOSED        0x00
#define SLCAN_STATUS_CAN_OPEN_NORMAL   0x01
#define SLCAN_STATUS_CAN_OPEN_LISTEN   0x01

#define SLCAN_FRAME_MAX_LENGTH         0x08
#define SLCAN_FRAME_MAX_SIZE           (sizeof("Tiiiiiiiildddddddddddddddd\r")+1)

#define SLCAN_OFF                      '0'
#define SLCAN_ON_ONE                   '1'
#define SLCAN_ON_TWO                   '2'

#define SLCAN_AUTOPOLL_OFF             0x00
#define SLCAN_AUTOPOLL_ON              0x01

#define SLCAN_AUTOSTART_OFF            0x00
#define SLCAN_AUTOSTART_ON_NORMAL      0x01
#define SLCAN_AUTOSTART_ON_LISTEN      0x02

#define SLCAN_TIMESTAMP_OFF            0x00
#define SLCAN_TIMESTAMP_ON_NORMAL      0x01
#define SLCAN_TIMESTAMP_ON_EXTENDED    0x02

#define SLCAN_OFFSET_STD_PKT_LEN       0x04
#define SLCAN_OFFSET_STD_PKT_DATA      0x05
#define SLCAN_OFFSET_EXT_PKT_LEN       0x09
#define SLCAN_OFFSET_EXT_PKT_DATA      0x0A


#define SLCAN_DEFAULT_BAUD_RATE        115200
#define SLCAN_CAN_BAUD_NUM             0x0a
#define SLCAN_UART_BAUD_NUM            0x07


#define CAN_5KBPS    1
#define CAN_10KBPS   2
#define CAN_20KBPS   3
#define CAN_31K25BPS 4
#define CAN_33KBPS   5
#define CAN_40KBPS   6
#define CAN_50KBPS   7
#define CAN_80KBPS   8
#define CAN_83K3BPS  9
#define CAN_95KBPS   10
#define CAN_100KBPS  11
#define CAN_125KBPS  12
#define CAN_200KBPS  13
#define CAN_250KBPS  14
#define CAN_500KBPS  15
#define CAN_1000KBPS 16

#define CAN_OK                  (0)
#define CAN_FAILINIT            (1)
#define CAN_FAILTX              (2)
#define CAN_MSGAVAIL            (3)
#define CAN_NOMSG               (4)
#define CAN_CTRLERROR           (5)
#define CAN_GETTXBFTIMEOUT      (6)
#define CAN_SENDMSGTIMEOUT      (7)
#define CAN_FAIL                (0xff)

#define CAN_MAX_CHAR_IN_MESSAGE (8)
    
// Function prototypes
    
typedef uint8_t (*slcan_send_frame_handler_ptr)(uint32_t id, uint8_t ext, uint8_t rtr, uint8_t len, uint8_t *buf);

void SLCAN_RegisterSendFrameHandler(slcan_send_frame_handler_ptr handler);

void SLCAN_Command(char * command);

uint8_t SLCAN_Receive(uint32_t id, uint8_t len, uint8_t* data);

uint8_t SLCAN_OutputPending();

uint8_t SLCAN_Read(uint8_t * buffer, uint8_t len);

//char * SLCAN_OutputData();
//void SLCAN_OutputFlush();
    
uint8_t SLCAN_Write(char * data, uint8_t len);
//void SLCAN_InputFlush();

//void SLCAN_Tasks(void);

#ifdef	__cplusplus
}
#endif

#endif	/* SLCAN_H */

