#include <stdint.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdlib.h>

#include "system_definitions.h"
#include "slcan.h"

static uint8_t _CanSpeedSelection = CAN_500KBPS;
static uint8_t _CanChannelMode = SLCAN_STATUS_CAN_CLOSED;
static uint8_t _LastErr = SLCAN_OK;

static uint8_t _AutoStart = SLCAN_AUTOSTART_OFF;
static uint8_t _AutoPoll  = SLCAN_AUTOPOLL_OFF;
static uint8_t _TimeStamp = SLCAN_TIMESTAMP_OFF;

static uint32_t _CanId = 0;

static uint8_t _Buffer[16];
static uint8_t _PacketLen = 0;

static uint8_t _InputLength = 0;

static uint8_t _OutputLength = 0;
static uint8_t _OutputHead = 0;
static uint8_t _OutputTail = 0;

static char _InputBuffer[128];
static char _OutputBuffer[248];

static uint8_t _Message[SLCAN_FRAME_MAX_SIZE];

const uint32_t _CanBaudRates[] //PROGMEM
= { CAN_10KBPS, CAN_20KBPS, CAN_50KBPS, 
    CAN_100KBPS, CAN_125KBPS, CAN_250KBPS, 
    CAN_500KBPS, CAN_500KBPS /*CAN_800KBPS*/,
    CAN_1000KBPS, CAN_83K3BPS };

const uint32_t _SerialBaudRates[] //PROGMEM
= { 230400, 115200, 57600, 38400, 19200, 9600, 2400 };

static slcan_send_frame_handler_ptr _frame_tx_handler = NULL;

//============================================================================

void SLCAN_RegisterSendFrameHandler(slcan_send_frame_handler_ptr handler)
{
    _frame_tx_handler = handler;
}

// Write into input buffer
uint8_t SLCAN_Write(char * data, uint8_t len) 
{
    uint8_t i, l;
    for (i=0, l=0; i < len; i++)
    {
        if (_InputLength < sizeof(_InputBuffer))
        {
            char chr = data[i];
            if (chr == SLCAN_CR)
            {
                _InputBuffer[_InputLength] = 0;
                SLCAN_Command(_InputBuffer);
                _InputLength = 0;
            }
            else
            {
                _InputBuffer[_InputLength] = chr;
                _InputLength++;
            }
            l++;
        }
    }
    return l;
}

//void SLCAN_InputFlush() { _InputLength = 0; }

uint8_t SLCAN_OutputPending() 
{
    //return _OutputLength; 
    uint8_t pending = (_OutputTail >= _OutputHead) ? 
        (_OutputTail - _OutputHead) : 
        (sizeof(_OutputBuffer) - _OutputHead + _OutputTail);
    return pending;
}

//char * SLCAN_OutputData() { return _OutputBuffer; }

//void SLCAN_OutputFlush() { _OutputLength = 0; }

uint8_t SLCAN_Read(uint8_t * buffer, uint8_t len) 
{
    uint8_t i;
    for (i=0; i < len && _OutputHead != _OutputTail; i++)
    {
        buffer[i] = _OutputBuffer[_OutputHead++];
        if (_OutputHead >= sizeof(_OutputBuffer)) // Roll Over
            _OutputHead = 0;
    }
    return i;
}

//-----------------------------------------------------------------------------

//void printFlush() {
//    _OutputBuffer[_OutputLength]=0;
//    //SYS_PRINT("%s", _OutputBuffer);
//    _OutputLength = 0;
//}

void printChar(char c) {
    //if (_OutputLength < sizeof(_OutputBuffer)-1) {
    //    _OutputBuffer[_OutputLength++]=c;
    //}
    
    uint8_t free = (_OutputTail >= _OutputHead) ? 
        (sizeof(_OutputBuffer) - _OutputTail + _OutputHead) : 
        (_OutputHead - _OutputTail);
    if (free > 1)
    {
        _OutputBuffer[_OutputTail++] = c;
        if (_OutputTail >= sizeof(_OutputBuffer))
            _OutputTail = 0;
    }
    ////SYS_PRINT("%c", c);
}

void printNibble(uint8_t b) {
    b &= 0xF;
    printChar(b < 10 ? '0'+b : 'A'+(b-10));
}

inline void printFullByte(uint8_t b) {
    printNibble(b >> 4);
    printNibble(b);
}

uint8_t parseNibble(uint8_t hex) {
    uint8_t ret = 0;
    if (hex >= '0' && hex <= '9') {
        ret = hex - '0';
    } else if (hex >= 'a' && hex <= 'f') {
        ret = hex - 'a' + 10;
    } else if (hex >= 'A' && hex <= 'F') {
        ret = hex - 'A' + 10;
    } // else error, return 0
    return ret;
}

inline uint8_t parseFullByte(uint8_t H, uint8_t L) {
    return (parseNibble(H) << 4) + parseNibble(L);
}

uint8_t parseNibbleWithLimit(uint8_t hex, uint8_t limit) {
    uint8_t ret = parseNibble(hex);
    if (ret <= limit)
        return ret;
    else
        return 0;
}

//----------------------------------------------

uint32_t parseCanStdId(char * cmd) {
    _CanId = (((uint32_t)parseNibble(cmd[1])) << 8)
        + (((uint32_t)parseNibble(cmd[2])) << 4)
        + (((uint32_t)parseNibble(cmd[3])));
    return _CanId;
}

uint32_t parseCanExtId(char * cmd) {
    _CanId = 
          (((uint32_t)parseNibble(cmd[1])) << 28)
        + (((uint32_t)parseNibble(cmd[2])) << 24)
        + (((uint32_t)parseNibble(cmd[3])) << 20)
        + (((uint32_t)parseNibble(cmd[4])) << 16)
        + (((uint32_t)parseNibble(cmd[5])) << 12)
        + (((uint32_t)parseNibble(cmd[6])) << 8)
        + (((uint32_t)parseNibble(cmd[7])) << 4)
        + (((uint32_t)parseNibble(cmd[8])));
    return _CanId;
}

//bool isExtendedFrame() { return false; }

bool checkPassFilter(uint32_t addr) { return true; }



//uint8_t checkReceive() 
//{
//    CAN_CHANNEL_EVENT channelEvent;
//    channelEvent = PLIB_CAN_ChannelEventGet(CAN232_ID, CAN232_CH_RX);
//    if((channelEvent & (CAN_RX_CHANNEL_NOT_EMPTY | CAN_RX_CHANNEL_FULL)) != 0) {
//        
//        //SYS_MESSAGE("\r\nRX\r\n");
//        
//        return CAN_MSGAVAIL;
//    }
//    return CAN_NOMSG; 
//}

//uint8_t readMsgBufID(uint32_t *ID, uint8_t *len, uint8_t buf[]) {
//    
//    uint8_t ret = CAN_FAIL;
//    
//    CAN_CHANNEL_EVENT channelEvent;
//    channelEvent = PLIB_CAN_ChannelEventGet(CAN232_ID, CAN232_CH_RX);
//
//    //if (channelEvent != 0)
//    //    //SYS_PRINT("\r\nCAN EVENT:%d", channelEvent);
//
//    if((channelEvent & (CAN_RX_CHANNEL_NOT_EMPTY | CAN_RX_CHANNEL_FULL)) != 0)
//    {
//        // This means that either Receive Channel  is not empty
//        // or the Channel is full.
//        
//        int i;
//        CAN_RX_MSG_BUFFER *RxMessage;     
//        RxMessage = (CAN_RX_MSG_BUFFER *)PLIB_CAN_ReceivedMessageGet(CAN232_ID, CAN232_CH_RX);
//        if(RxMessage != NULL)
//        {
//#ifdef TRACE
//            //SYS_PRINT("\r\nSID:%04X L=%d ", RxMessage->msgSID.sid, RxMessage->msgEID.data_length_code);
//#endif      
//            *ID = RxMessage->msgSID.sid;
//            *len = RxMessage->msgEID.data_length_code;
//            for (i=0; i<RxMessage->msgEID.data_length_code; i++) {
//                buf[i] = RxMessage->data[i];
//#ifdef TRACE
//                //SYS_PRINT("%02X",buf[i]);
//#endif
//            }
//#ifdef TRACE
//            //SYS_PRINT("\r\n");
//#endif
//            
//            //Process the message received
//
//            /* Call the PLIB_CAN_ChannelUpdate function to let
//            the CAN module know that the message processing
//            is done. */
//            PLIB_CAN_ChannelUpdate(CAN232_ID, CAN232_CH_RX);
//
//            ret = CAN_OK;
//        }
//        
//        PLIB_CAN_ChannelEventClear(CAN232_ID, CAN232_CH_RX, channelEvent);
//    }
//    return ret; 
//}


uint8_t SLCAN_Receive(uint32_t id, uint8_t len, uint8_t* data) 
{
    if (_CanChannelMode == SLCAN_STATUS_CAN_CLOSED)
        return 1;
    
    uint8_t ret = SLCAN_OK;
    uint8_t idx = 0;
    
    _CanId = id;
    _PacketLen = len > 8 ? 8 : len;
    //memcpy(_Buffer, data, len);
    
    ////SYS_PRINT("\r\nSLCAN RX %03X %d", id, len);
    
    //if (CAN_OK == readMsgBufID(&_CanId, &_PacketLen, _Buffer)) 
    //{
    if (_CanId > 0x1FFFFFFF) {
        ret = SLCAN_ERR; // address if totally wrong
    }
    else if (checkPassFilter(_CanId)) { // do we want to skip some addresses?
        if (id >= 0x8000) //(isExtendedFrame()) 
        {
            printChar(SLCAN_TR29);
            printFullByte(HIGH_BYTE(HIGH_WORD(_CanId)));
            printFullByte(LOW_BYTE(HIGH_WORD(_CanId)));
            printFullByte(HIGH_BYTE(LOW_WORD(_CanId)));
            printFullByte(LOW_BYTE(LOW_WORD(_CanId)));
        }
        else {
            printChar(SLCAN_TR11);
            printNibble(HIGH_BYTE(LOW_WORD(_CanId)));
            printFullByte(LOW_BYTE(LOW_WORD(_CanId)));
        }
        //write data len
        printNibble(_PacketLen);
        //write data
        for (idx = 0; idx < _PacketLen; idx++) {
            printFullByte(data[idx]);
        }
        //write timestamp if needed
        if (_TimeStamp != SLCAN_TIMESTAMP_OFF) {
            // TODO
            uint32_t time = 0; //millis();
            if (_TimeStamp == SLCAN_TIMESTAMP_ON_NORMAL) { 
                // standard LAWICEL protocol. two bytes.
                time %= 60000;  
            } else {
                // non standard protocol - 4 bytes timestamp
                printFullByte(HIGH_BYTE(HIGH_WORD(time)));
                printFullByte(LOW_BYTE(HIGH_WORD(time)));
            }
            printFullByte(HIGH_BYTE(LOW_WORD(time)));
            printFullByte(LOW_BYTE(LOW_WORD(time)));
        }
    }
    printChar(SLCAN_CR);
    //}
    
    //printFlush();
    
    //else {
    //    ret = SLCAN_ERR;
    //}
    return ret;
}


uint8_t openCanBus() {
    return 0;
}

uint8_t closeCanBus() {
    return 0;
}

uint8_t checkError(uint8_t * err) { return 0; };
    
static uint8_t send_frame(uint32_t id, uint8_t ext, uint8_t rtr, uint8_t len, uint8_t *buf)
{ 
    ////SYS_PRINT("\r\nTX id:%X l:%d %02X R:%d", id, len, buf[0], rtr);
    if (_frame_tx_handler)
        return _frame_tx_handler(id, ext, rtr, len, buf);
    return CAN_OK;
}

//==============================================================================
// This function performs background tasks:
// - receives and displays frames in auto
// poll mode
//------------------------------------------------------------------------------
void SLCAN_Tasks()
{
    if (_CanChannelMode != SLCAN_STATUS_CAN_CLOSED
        && _AutoPoll == SLCAN_AUTOPOLL_ON) 
    {
        /*
        int recv = 0;
        while (CAN_MSGAVAIL == checkReceive() && recv++<5) {
            //printChar('+');
            if (CAN_OK == receiveSingleFrame()) {
                printChar(SLCAN_CR);
            }
        }
        */
    }
    
    //if (_OutputLength > 0)
    //    printFlush();
}

//uint8_t * SCLAN_OutputBuffer(void) {
//    return _OutputBuffer;
//}
//
//int SCLAN_OutputPending(uint8_t ** dptr) {
//    if (_OutputLength > 0)
//        *dptr = _OutputBuffer;
//    return _OutputLength;
//}
//
//void SCLAN_OutputFlush(void) {
//    _OutputLength = 0;
//}

//==============================================================================
// Parses and executes a command
// Returns: SLCAN_OK in case of success, otherwise - specific error code
//------------------------------------------------------------------------------
void SLCAN_Command(char * command) {
    ////SYS_PRINT(">%s", command);
    
    uint8_t ret = SLCAN_OK;
    uint8_t idx = 0;
    uint8_t err = 0;

    _LastErr = SLCAN_OK;
    
    ////SYS_PRINT("\r\nSLCAN cmd=%c", command[0]);

    switch (command[0]) {
        case SLCAN_CMD_SETUP:
            // Sn[CR] Setup with standard CAN bit-rates where n is 0-9.
            if (_CanChannelMode == SLCAN_STATUS_CAN_CLOSED) {
                idx = parseNibbleWithLimit(command[1], SLCAN_CAN_BAUD_NUM);
                _CanSpeedSelection = _CanBaudRates[idx];
                
            }
            else {
                ret = SLCAN_ERR;
            }
            break;
        case SLCAN_CMD_SETUP_BTR:
            // sxxyy[CR] Setup with BTR0/BTR1 CAN bit-rates where xx and yy is a hex value.
            ret = SLCAN_ERR; break;
        case SLCAN_CMD_OPEN:
            // O[CR] Open the CAN channel in normal mode (sending & receiving).
            if (_CanChannelMode == SLCAN_STATUS_CAN_CLOSED) {
                _CanChannelMode = SLCAN_STATUS_CAN_OPEN_NORMAL;
                ret = openCanBus();
            }
            else {
                ret = SLCAN_ERR;
            }
            break;
        case SLCAN_CMD_LISTEN:
            // L[CR] Open the CAN channel in listen only mode (receiving).
            if (_CanChannelMode == SLCAN_STATUS_CAN_CLOSED) {
                _CanChannelMode = SLCAN_STATUS_CAN_OPEN_LISTEN;
                ret = openCanBus();
            }
            else {
                ret = SLCAN_ERR;
            }
        break;
        case SLCAN_CMD_CLOSE:
            // C[CR] Close the CAN channel.
            if (_CanChannelMode != SLCAN_STATUS_CAN_CLOSED) {
                _CanChannelMode = SLCAN_STATUS_CAN_CLOSED;
                closeCanBus();
            }
            else {
                ret = SLCAN_ERR;
            }
        break;
        case SLCAN_CMD_TX11:
            // tiiildd...[CR] Transmit a standard (11bit) CAN frame.
            if (_CanChannelMode == SLCAN_STATUS_CAN_OPEN_NORMAL) {
                _CanId = parseCanStdId(command);
                _PacketLen = parseNibbleWithLimit(command[SLCAN_OFFSET_STD_PKT_LEN], SLCAN_FRAME_MAX_LENGTH);
                for (idx=0; idx < _PacketLen; idx++) {
                    _Buffer[idx] = parseFullByte(command[SLCAN_OFFSET_STD_PKT_DATA + idx * 2], 
                                                 command[SLCAN_OFFSET_STD_PKT_DATA + idx * 2 + 1]);
                }
                if (CAN_OK != send_frame(_CanId, 0, 0, _PacketLen, _Buffer)) {
                    ret = SLCAN_ERR;
                }
                else if (_AutoPoll) {
                    ret = SLCAN_OK_SMALL;
                }
            }
            else {
                ret = SLCAN_ERR;
            }
        break;
    case SLCAN_CMD_TX29:
        // Tiiiiiiiildd...[CR] Transmit an extended (29bit) CAN frame
        if (_CanChannelMode == SLCAN_STATUS_CAN_OPEN_NORMAL) {
            _CanId = parseCanExtId(command);
            _PacketLen = parseNibbleWithLimit(command[SLCAN_OFFSET_EXT_PKT_LEN], SLCAN_FRAME_MAX_LENGTH);
            for (idx=0; idx < _PacketLen; idx++) {
                _Buffer[idx] = parseFullByte(command[SLCAN_OFFSET_EXT_PKT_DATA + idx * 2],
                                             command[SLCAN_OFFSET_EXT_PKT_DATA + idx * 2 + 1]);
            }
            if (CAN_OK != send_frame(_CanId, 1, 0, _PacketLen, _Buffer)) {
                ret = SLCAN_ERR;
            } else if (_AutoPoll) {
                ret = SLCAN_OK_BIG;
            }
        }
        break;
    case SLCAN_CMD_RTR11:
        // riiil[CR] Transmit an standard RTR (11bit) CAN frame.
        if (_CanChannelMode == SLCAN_STATUS_CAN_OPEN_NORMAL) {
            parseCanStdId(command);
            _PacketLen = parseNibbleWithLimit(command[SLCAN_OFFSET_STD_PKT_LEN], SLCAN_FRAME_MAX_LENGTH);
            if (CAN_OK != send_frame(_CanId, 0, 1, _PacketLen, _Buffer)) {
                ret = SLCAN_ERR;
            }
            else if (_AutoPoll) {
                ret = SLCAN_OK_SMALL;
            }
        }
        else {
            ret = SLCAN_ERR;
        }
        break;
    case SLCAN_CMD_RTR29:
        // Riiiiiiiil[CR] Transmit an extended RTR (29bit) CAN frame.
        if (_CanChannelMode == SLCAN_STATUS_CAN_OPEN_NORMAL) {
            parseCanExtId(command);
            _PacketLen = parseNibbleWithLimit(command[SLCAN_OFFSET_EXT_PKT_LEN], SLCAN_FRAME_MAX_LENGTH);
            if (CAN_OK != send_frame(_CanId, 1, 1, _PacketLen, _Buffer)) {
                ret = SLCAN_ERR;
            }
            else if (_AutoPoll) {
                ret = SLCAN_OK_SMALL; // not a typo. strangely can232_v3.pdf tells to return "z[CR]", not "Z[CR]" as in 29bit. todo: check if it is error in pdf???
            }
        } else {
            ret = SLCAN_ERR;
        }
        break;
    case SLCAN_CMD_POLL_ONE:
        // P[CR] Poll incomming FIFO for CAN frames (single poll)
        if (_CanChannelMode != SLCAN_STATUS_CAN_CLOSED && _AutoPoll == SLCAN_AUTOPOLL_OFF) {
//            if (CAN_MSGAVAIL == checkReceive()) {
//                ret = receiveSingleFrame();
//            }
        } else {
            ret = SLCAN_ERR;
        }
        break;
    case SLCAN_CMD_POLL_MANY:
        // A[CR] Polls incomming FIFO for CAN frames (all pending frames)
        if (_CanChannelMode != SLCAN_STATUS_CAN_CLOSED && _AutoPoll == SLCAN_AUTOPOLL_OFF) {
//            while (CAN_MSGAVAIL == checkReceive()) {
//                ret = ret ^ receiveSingleFrame();
//                if (ret != CAN_OK)
//                    break;
//                printChar(SLCAN_CR);
//            }
//            if (ret == CAN_OK)
                printChar(SLCAN_ALL);
        } else {
            ret = SLCAN_ERR;
        }
        break;
    case SLCAN_CMD_FLAGS:
        // F[CR] Read Status Flags.
        // LAWICEL CAN232 and CANUSB have some specific errors which differ from MCP2515/MCP2551 errors. We just return MCP2515 error.
        printChar(SLCAN_FLAG);
        if (checkError(&err) == CAN_OK) 
            err = 0;
        printFullByte(err & 0xFF); //MCP_EFLG_ERRORMASK);
        break;
    case SLCAN_CMD_AUTOPOLL:
        // Xn[CR] Sets Auto Poll/Send ON/OFF for received frames.
        if (_CanChannelMode == SLCAN_STATUS_CAN_CLOSED) {
            _AutoPoll = (command[1] == SLCAN_ON_ONE) ? SLCAN_AUTOPOLL_ON : SLCAN_AUTOPOLL_OFF;
            //todo: save to eeprom
        } else {
            ret = SLCAN_ERR;
        }
        break;
    case SLCAN_CMD_FILTER:
        // Wn[CR] Filter mode setting. By default CAN232 works in dual filter mode (0) and is backwards compatible with previous CAN232 versions.
        ret = SLCAN_ERR_NOT_IMPLEMENTED; break;
    case SLCAN_CMD_ACC_CODE:
        // Mxxxxxxxx[CR] Sets Acceptance Code Register (ACn Register of SJA1000). // we use MCP2515,
        ret = SLCAN_ERR_NOT_IMPLEMENTED; break;
    case SLCAN_CMD_ACC_MASK:
        // mxxxxxxxx[CR] Sets Acceptance Mask Register (AMn Register of SJA1000).
        ret = SLCAN_ERR_NOT_IMPLEMENTED; break;
    case SLCAN_CMD_UART:
        // Un[CR] Setup UART with a new baud rate where n is 0-6.
        idx = parseNibbleWithLimit(command[1], SLCAN_UART_BAUD_NUM);
        //SYS_PRINT("%d",_SerialBaudRates[idx]);
        break;
    case SLCAN_CMD_VERSION1:
    case SLCAN_CMD_VERSION2:
        // V[CR] Get Version number of both CAN232 hardware and software
        //SYS_PRINT(SLCAN_VERSION_STR);
        break;
    case SLCAN_CMD_SERIAL:
        // N[CR] Get Serial number of the CAN232.
        //SYS_PRINT(SLCAN_SERIAL_NUM);
        break;
    case SLCAN_CMD_TIMESTAMP:
        // Zn[CR] Sets Time Stamp ON/OFF for received frames only. Z0 - OFF, Z1 - Lawicel's timestamp 2 bytes, Z2 - arduino timestamp 4 bytes.
        if (_CanChannelMode == SLCAN_STATUS_CAN_CLOSED) {
            // _TimeStamp = (command[1] == SLCAN_ON_ONE); 
            if (command[1] == SLCAN_ON_ONE) {
                _TimeStamp = SLCAN_TIMESTAMP_ON_NORMAL;
            }
            else if (command[1] == SLCAN_ON_TWO) {
                _TimeStamp = SLCAN_TIMESTAMP_ON_EXTENDED;
            }
            else {
                _TimeStamp = SLCAN_TIMESTAMP_OFF;
            }
        }
        else {
            ret = SLCAN_ERR;
        }
        break;
    case SLCAN_CMD_AUTOSTART:
        // Qn[CR] Auto Startup feature (from power on).
        if (_CanChannelMode != SLCAN_STATUS_CAN_CLOSED) {
            if (command[1] == SLCAN_ON_ONE) {
                _AutoStart = SLCAN_AUTOSTART_ON_NORMAL;
            }
            else if (command[1] == SLCAN_ON_TWO) {
                _AutoStart = SLCAN_AUTOSTART_ON_LISTEN;
            }
            else {
                _AutoStart = SLCAN_AUTOSTART_OFF;
            }
            //todo: save to eeprom
        }
        else {
            ret = SLCAN_ERR;
        }
        break;
    default:
        ret = SLCAN_ERR_UNKNOWN_CMD;
    }

    
    _LastErr = ret; //parseAndRunCommand();

    switch (_LastErr) {
    case SLCAN_OK:
        printChar(SLCAN_RET_ASCII_OK);
        break;
    case SLCAN_OK_SMALL:
        printChar(SLCAN_RET_ASCII_OK_SMALL);
        printChar(SLCAN_RET_ASCII_OK);
        break;
    case SLCAN_OK_BIG:
        printChar(SLCAN_RET_ASCII_OK_BIG);
        printChar(SLCAN_RET_ASCII_OK);
        break;
    case SLCAN_ERR_NOT_IMPLEMENTED:
        // Choose behavior: will it fail or not when not implemented command comes in. Some can monitors might be affected by this selection.
        printChar(SLCAN_RET_ASCII_ERROR);
        ////SYS_PRINT(SLCAN_RET_ASCII_OK); 
        break;
    default:
        printChar(SLCAN_RET_ASCII_ERROR);
    }
}
